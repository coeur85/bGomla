﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace bGomlaDB
{

    public partial class Machien
    {

        private System.Net.NetworkInformation.PingReply PingReply;
        public System.Net.NetworkInformation.PingReply Ping
        {
            get
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                if (PingReply == null)
                {
                    PingReply = ping.Send(IP);
                }

                return PingReply;

            }
        }


    }
    public partial class dbServer
    {

        public bool Online
        {
            get
            {

                if (Machien.Ping.Status == System.Net.NetworkInformation.IPStatus.Success) { return true; }
                else return false;

            }
        }


       

        public List<Subscrtion> PublicationsSubscriptions
        {
            get
            {
                List<Subscrtion> sl = new List<Subscrtion>();
                foreach (var pub in ServerPublications)
                {

                    sl.AddRange(pub.Subscrtions.Where(x => !sl.Any(y => x.SubscriperServerID ==
                    x.SubscriperServerID)).ToList());

                }

                return sl;
            }

        }
    }
    public static class TicketStatus
    {
        public const int Underinvestegation = 0;
        public const int Open = 1;
        public const int Closed = 2;

    }

    public partial class MachinePerformanceCounter {


        public string RamString { get { return String.Format("{0} MB", Ram); } }
        public string CPUString { get { return String.Format("{0:##0} %", CPU); } }

    }
}