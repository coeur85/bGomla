//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bGomlaDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Backup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Backup()
        {
            this.BackupLocations = new HashSet<BackupLocation>();
        }
    
        public int BackupID { get; set; }
        public int BackupTypeID { get; set; }
        public int dbServerID { get; set; }
        public int MinuteInterval { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BackupLocation> BackupLocations { get; set; }
        public virtual BackupType BackupType { get; set; }
        public virtual dbServer dbServer { get; set; }
    }
}
