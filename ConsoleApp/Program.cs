﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {

        static string domin = "http://" + ConfigurationSettings.AppSettings.Get("DomainUrl") + "/api/";
        static string user = ConfigurationSettings.AppSettings.Get("DomainUser") ;
        static string password = ConfigurationSettings.AppSettings.Get("DomainUserPassword");
        private static string SendRequest(string  action , string controller)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(domin + controller +"/" + action);
            Console.WriteLine(request.RequestUri.AbsolutePath);
            request.Timeout = 9240000;
            request.Method = "GET";
            request.UseDefaultCredentials = false;
            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(user, password, "bgomla");

           // HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader stIn = new StreamReader(request.GetResponse().GetResponseStream());
            var strResponse = stIn.ReadToEnd();
            stIn.Close();
            return strResponse;
          //  return response.get
        }

        static void Main(string[] args)
        {

            if (args.FirstOrDefault() == "n") { Network(); }
            else if (args.FirstOrDefault() == "bf") { Backup("Full"); }
            else if (args.FirstOrDefault() == "bd") { Backup("Differential"); }
            else if (args.FirstOrDefault() == "bt") { Backup("Transaction"); }
            else if (args.FirstOrDefault() == "p") { Preformance(); }
            else if (args.FirstOrDefault() == "sd") { DailySales(); }

        }


        public static void Network() {
            Console.WriteLine("called network method") ;

            //// var s = new System.Net.WebClient().DownloadString(domin + "Network/Servers");

            ////HttpWebRequest request = (HttpWebRequest)WebRequest.Create(domin + "Network/Servers");
            ////string authInfo = "bgomla\ahmed.magdi:0103061";
            ////authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            ////request.Headers.Add("Authorization", "Basic " + authInfo);
            ////request.Credentials = new NetworkCredential("bgomla\ahmed.magdi", "0103061");
            ////request.Method = WebRequestMethods.Http.Get;
            ////request.AllowAutoRedirect = true;
            ////request.Proxy = null;
            ////HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            ////Stream stream = response.GetResponseStream();
            ////StreamReader streamreader = new StreamReader(stream);
            ////string s = streamreader.ReadToEnd();


            //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(domin + "Network/Servers");

            //request.Method = "GET";
            //request.UseDefaultCredentials = false;
            //request.PreAuthenticate = true;
            //request.Credentials = new NetworkCredential("ahmed.magdi", "0103061", "bgomla");

            var response = SendRequest("Servers","Network");

            Console.WriteLine(response);
            Console.WriteLine("Done");
        }
        public static void Backup(string type)
        {
           Console.WriteLine("called " + type + " backup method");
           var response = SendRequest(type, "Backup");
            Console.WriteLine(response);
            Console.WriteLine("Done");
        }
        public static void Preformance() {
            Console.WriteLine("called Preformance method");
            var response = SendRequest("Servers", "Preformance");
            Console.WriteLine(response);
            Console.WriteLine("Done");

        }
        public static void DailySales() {
            Console.WriteLine("called daily sales method");
            var response = SendRequest("SendDailyReport", "Sales");
            Console.WriteLine(response);
            Console.WriteLine("Done");
        }

    }

}
