﻿
class backUps {

    constructor(id) {
        this.id = id;
        this.box = $('#buBox_' + this.id);
        this.bar = $('#bubar_' + this.id);
        this.barinner = $('#bubarInner_' + this.id);
        this.plans = $('#plans_' + this.id);
        this.btn = $('#bubtnRefresh_' + this.id);

    } 

    progBarUI(status) {

     
        if (status === 3) {

            this.box.removeClass('green').removeClass('red').addClass('yellow');
            this.barinner.removeClass('progress-bar-danger').removeClass('progress-bar-success').addClass('progress-bar-warning');
            this.bar.addClass('progress-striped');
            this.barinner.html(' Working ...!');

            this.btn.slideUp();
            this.plans.slideUp();
           

        }
        if (status === 1) {

            this.box.addClass('green').removeClass('yellow').removeClass('red');
            this.barinner.removeClass('progress-bar-danger').removeClass('progress-bar-warning').addClass('progress-bar-success');
            this.bar.removeClass('progress-striped');
            this.barinner.html('Server backups are vailed');
            this.btn.slideDown();
            this.plans.slideDown();

        }
        if (status === 0) {

            this.box.removeClass('yellow').removeClass('green').addClass('red');
            this.barinner.removeClass('progress-bar-success').removeClass('progress-bar-warning').addClass('progress-bar-danger');
            this.bar.removeClass('progress-striped');
            this.barinner.html('Server has a backup issue');
            this.btn.slideDown();
            this.plans.slideDown();
        }
        if (status === 2) {

            this.box.removeClass('green').removeClass('red').addClass('yellow');
            this.barinner.removeClass('progress-bar-danger').removeClass('progress-bar-warning').addClass('progress-bar-success');
            this.bar.removeClass('progress-striped');
            this.barinner.html('Server has a backup warining');
            this.btn.slideDown();
            this.plans.slideDown();
        }
    }
    loadSccuess() {
        this.progBarUI(1);
        console.log('suc');
    }
    loadWarning() {
        this.progBarUI(2);
    }
    loadFailed() {
        this.progBarUI(0);

    }
    LoadBackups() {
        
        var subid = this.id;
        this.progBarUI(3);

        ajaxGet('ServerBackup', 'it/Backup', this.id, function (d) {

            var s = new backUps(subid);
            s.plans.html(d.Data);
            console.log(s.id + ' is ' + d.Status);
            if (d.Status === 1) { s.loadSccuess(); }
            else if (d.Status === 2) { s.loadWarning(); }
            else if (d.Status === 0) { s.loadFailed(); }
            initTimeAgo();

        }, function () { });

    }

}


