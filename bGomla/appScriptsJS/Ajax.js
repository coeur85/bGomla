var AjaxHelper;
(function (AjaxHelper) {
    var Subscription = /** @class */ (function () {
        function Subscription() {
            this.className = '';
        }
        Subscription.prototype.callWebApi = function (url, data, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onload = function () {
                var data = JSON.parse(xhr.responseText);
                callback(data);
            };
            xhr.onerror = function () {
                alert("Error while calling Web API");
            };
            xhr.open('POST', url);
            xhr.setRequestHeader("Content-Type", "application/json");
            if (data == null) {
                xhr.send();
            }
            else {
                xhr.send(JSON.stringify(data));
            }
        };
        return Subscription;
    }());
    AjaxHelper.Subscription = Subscription;
})(AjaxHelper || (AjaxHelper = {}));
//# sourceMappingURL=Ajax.js.map