var ServerSubscription = /** @class */ (function () {
    function ServerSubscription(name) {
        this.name = name;
        this.hello = function () {
            alert('hello ' + this.name + ' !');
        };
    }
    return ServerSubscription;
}());
//# sourceMappingURL=Replication.js.map