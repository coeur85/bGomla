﻿using System.Web.Mvc;

namespace bGomla.Areas.it
{
    public class itAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "it";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "it_default",
                "it/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}