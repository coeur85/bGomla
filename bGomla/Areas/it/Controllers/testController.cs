﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bGomlaDB;
using System.Management;

namespace bGomla.Areas.it.Controllers
{
    public class testController : Controller
    {
        // GET: test
        bGomlaDBEntities db = new bGomlaDBEntities();
       

        [AllowAnonymous]
        public ActionResult Index()
        {

            var allTick = db.Tickets.Where(x => x.TicketTypeID == 1).ToList();

            foreach (var tic in allTick.Where(x=> !x.DateClosed.HasValue).ToList())
            {
                tic.DateClosed = DateTime.Now;
            }


            List<TicketEvent> te = new List<TicketEvent>();


            te = (from x in allTick.Take(100).ToList() select new TicketEvent {
                 backgroundColor = x.Machien.dbServer.Branch.rgba,
                 end = x.DateClosed.Value,
                 start = x.DateOpen,
                 title = x.Machien.dbServer.Branch.Name + " for " + _app.UI.TimeSpanToString(x.DateClosed.Value  - x.DateOpen)
                 
            }).ToList();

            return View(te);
          
        }
        
    }




  
    

    
    public class TicketEvent
    {

        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public bool allDay { get { return false; } }
        public string backgroundColor { get; set; }
    }
}