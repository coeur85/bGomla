﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bGomlaDB;

namespace bGomla.Areas.it.Controllers
{
    public class MonitorController : Controller
    {
        // GET: Monitor
        bGomlaDBEntities db = new bGomlaDBEntities();
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult OutByBranchLocation(int id)
        {
            return View(db.Branches.Find(id));
        }

        public ActionResult InByBranchLocation(int id)
        {
            return View(db.Branches.Find(id));
        }


        public ActionResult Publication(int id)
        {
            return View(db.Publications.Find(id));
        }
    }
}