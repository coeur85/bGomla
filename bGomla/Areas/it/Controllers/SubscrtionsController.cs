﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bGomlaDB;
using System.Net.NetworkInformation;
using System.IO;

namespace bGomla.Areas.it.Controllers
{
    public class SubscrtionsController : Controller
    {
        bGomlaDB.bGomlaDBEntities db = new bGomlaDB.bGomlaDBEntities();
        _app.ADO ado = new _app.ADO();
        // GET: Subscrtions      
        public ActionResult SubResponse(int id)
        {
           // System.Threading.Thread.Sleep(5000);
            var sub = db.Subscrtions.Find(id);
            _app.WebAPI.Response response = new _app.WebAPI.Response();
            if (sub.ServerPublication.dbServer.Online && sub.SubscriptionServer.Online)
            {
                List<_app.PagesModel.Artical> art = new List<_app.PagesModel.Artical>();
               
                foreach (var item in sub.ServerPublication.Publication.Articals.ToList())
                {

                    Branch br = new Branch();

                    if (item.PubID == 18) // purchars order
                    { br = sub.SubscriptionServer.Branch; }
                    else
                    { br = sub.ServerPublication.dbServer.Branch; }



                    art.Add(new _app.PagesModel.Artical
                    {
                        ArticalName = item.ArticalName,
                        PublisherCount = ado.RowCount(item.ArticalName, item.Fillter, sub.ServerPublication.dbServer.ConnectionString,
                        br),
                        SubScriperCount = ado.RowCount(item.ArticalName, item.Fillter, sub.SubscriptionServer.ConnectionString,
                        br),
                        ArticalID = item.ArtID,
                        SubID = sub.SubscriptionID,
                        GroupBy = item.GroupBy
                    });

                }

                var PubCount = art.Sum(x => x.PublisherCount);
                var SubsCount = art.Sum(x => x.SubScriperCount);


                if (PubCount - SubsCount == 0) { response.Status = 1; }
                else { response.Status = 2; }


                // response.Data = _app.FakeController.RenderViewToString( PartialView("~/Views/Shared/Replication/Subscription/_Articals.cshtml", art);
                response.Data = ConvertPartialViewToString(PartialView("~/Areas/it/Views/Shared/Replication/Subscription/_Articals.cshtml", art));

            }

            else
            {
                _app.WebAPI.OfflineServers offlineServers = new _app.WebAPI.OfflineServers {
                     Publiser = sub.ServerPublication.dbServer,
                     Subscriper = sub.SubscriptionServer
                };

                response = new _app.WebAPI.Response { Status = 0, Data = 
                    ConvertPartialViewToString(PartialView("~/Areas/it/Views/Shared/Replication/Subscription/_Offline.cshtml", offlineServers)) };

            }


            return Json(response,JsonRequestBehavior.AllowGet);
        }
        public ActionResult GroupData(int id , int articalID)
        {

            var sub = db.Subscrtions.Find(id);
            var pubList = ado.GroupedDatas(sub.ServerPublication.dbServer.Branch, db.Articals.Find(articalID), sub.ServerPublication.dbServer.ConnectionString);
            var subList = ado.GroupedDatas(sub.ServerPublication.dbServer.Branch, db.Articals.Find(articalID), sub.SubscriptionServer.ConnectionString);

            //  List<_app.WebAPI.GroupedData> result = new List<_app.WebAPI.GroupedData>();
            _app.PagesModel.GroupedData result = new _app.PagesModel.GroupedData();

            foreach (var item in pubList)
            {
                var s = subList.FirstOrDefault(x => x.RowDate == item.RowDate);
                if (s == null)
                {
                    result.GroupedDatas.Add(new _app.WebAPI.GroupedData
                    {
                        RowDate = item.RowDate,
                        Count = 0 - item.Count
                    });

                }
                else
                {
                    if (s.Count != item.Count)
                        {
                            result.GroupedDatas.Add(new _app.WebAPI.GroupedData { RowDate = item.RowDate ,
                                Count = s.Count - item.Count });
                        }

                }

                            
            }


            var art = db.Articals.Find(articalID);
            result.NewFilter = _app.ADO.Filter(art.Fillter, sub.ServerPublication.dbServer.Branch)
                .Replace("where" , "")
                + " and (" +
                art.GroupBy + " >= " + _app.ADO.SqlDateformate(DateTime.Now)  +" or " + art.GroupBy +" in (";

            foreach (var item in result.GroupedDatas)
            {
                result.NewFilter += _app.ADO.SqlDateformate(item.RowDate)  + "," ;

            }

            result.NewFilter = result.NewFilter.Remove(result.NewFilter.Length - 1, 1);
            result.NewFilter += "))";
           
            result.SubID = sub.SubscriptionID;
            return PartialView("~/Areas/it/Views/Shared/Replication/Subscription/_GroupedData.cshtml", result);
        }
      
        protected string ConvertPartialViewToString(PartialViewResult partialView)
        {
            using (var sw = new StringWriter())
            {
                partialView.View = ViewEngines.Engines
                  .FindPartialView(ControllerContext, partialView.ViewName).View;

                var vc = new ViewContext(
                  ControllerContext, partialView.View, partialView.ViewData, partialView.TempData, sw);
                partialView.View.Render(vc, sw);

                var partialViewString = sw.GetStringBuilder().ToString();

                return partialViewString;
            }
        }
    }
}