﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bGomlaDB;
using Microsoft.Ajax.Utilities;

namespace bGomla.Areas.it.Controllers
{
    public class NetworkController : Controller
    {
        // GET: Network
        bGomlaDBEntities db = new bGomlaDBEntities();
        public ActionResult Index()
        {
            var id = 1;
            
            var allTick = db.Tickets.Where(x => x.TicketTypeID == id).ToList();
        
            foreach (var item in allTick.Where(x=> !x.DateClosed.HasValue).ToList())
            { item.DateClosed = DateTime.Now; }


            List<Ticket> gaps = new List<Ticket>();
            foreach (var item in allTick.Where(x=> x.DateOpen.Date != x.DateClosed.Value.Date).ToList())
            {
                for (DateTime i = item.DateOpen; i <= item.DateClosed.Value; i = i.AddDays(1))
                {
                    gaps.Insert(gaps.Count,new Ticket { MachineID = item.MachineID, DateOpen = i.Date.AddDays(1), Machien = item.Machien,
                        DateClosed = i.Date.AddDays(2).AddSeconds(1), StatusID = item.StatusID, TicketTypeID = item.TicketTypeID });

                    if (gaps.LastOrDefault().DateClosed.Value.Date == item.DateClosed.Value.Date)
                    { gaps.LastOrDefault().DateClosed = item.DateClosed; }

                }


                if (item.DateClosed.Value.Date != DateTime.Today)
                {
                    item.DateClosed = item.DateOpen.Date;
                    item.DateClosed = item.DateClosed.Value.AddDays(1).AddSeconds(1);

                }
                else
                { gaps.LastOrDefault().DateClosed = DateTime.Now; }
            }

            allTick.AddRange(gaps);

            var tt = db.TicketTypes.Find(id);
            _app.PagesModel.TicketTypeHomePage hp = new _app.PagesModel.TicketTypeHomePage
            {
                TypeName = tt.Name,
                From = allTick.OrderBy(x => x.DateOpen).FirstOrDefault().DateOpen,
                TO = DateTime.Now
            };


            var qChart = allTick.Select(x => new {
                server = x.Machien.dbServer.Branch.Name,
                period =
                 (x.DateClosed - x.DateOpen)
            }).ToList().GroupBy(x => x.server).
                 Select(x => new { X = x.Key, Y = x.Sum(y => y.period.Value.Ticks) }).ToList();

            //  qChart = qChart.Where(x => (x.Y/60) > 0).OrderBy(x=> x.Y).ToList();



            hp.Chart1 = new _app.PagesModel.ChartClass
            {
                CanvesID = 1,
                ChartType = "line",
              //  Color = "blue",
              //  Data = qChart.Select(x => Math.Round(new TimeSpan(x.Y).TotalHours, 0).ToString()).ToArray(),
             //   label = "about",
                Lables = qChart.Select(x => x.X).ToArray(),
                PointTilite = " hours",
                text = tt.Message,
                xAexTitle = "Branch",
                yAexTitle = "Peroid",
                ChartDataSets = new List<_app.PagesModel.ChartDataSet> { new _app.PagesModel.ChartDataSet {
                    Color = "blue", label = "about", Data= qChart.Select(x => Math.Round(new TimeSpan(x.Y).TotalHours, 0).ToString()).ToArray() } }

            };


            //-------------- offline table --------------------------

            hp.offlineServersTable = new _app.PagesModel.OfflineServersTable
            {
                dbServers =
                allTick.DistinctBy(x => x.Machien.MachienID).Select(x => x.Machien.dbServer).ToList()
            };


            var datefrom = allTick.Min(x => x.DateOpen);
            var dateto = allTick.Max(x => x.DateOpen);

            for (DateTime date = dateto; date.Date >= datefrom.Date; date = date.AddDays(-1))
            {

                hp.offlineServersTable.offlineServerDayPeroids.Add(new _app.PagesModel.OfflineServerDayPeroid { Date = date.Date });


                foreach (var srv in hp.offlineServersTable.dbServers)
                {

                    hp.offlineServersTable.offlineServerDayPeroids.LastOrDefault()
                        .Peroid.Add(new TimeSpan(allTick.Where(x => x.MachineID == srv.ServerID
                        ).Select(x => new { date = x.DateOpen.Date, peroid = x.DateClosed - x.DateOpen })
                        .GroupBy(x => x.date).Select(x => new {
                            date = x.Key,
                            peroid
                        = x.Sum(y => y.peroid.Value.Ticks)
                        }).Where(x => x.date.Date == date.Date).Sum(x => x.peroid)));

                }

            }


            //--- end of offline table
            foreach (var srv in db.dbServers.ToList())
            {
                hp.OnlineServers.Add(new _app.PagesModel.onlineServers { name = srv.Branch.Name, online = srv.Online });
            }


            return View(hp);
        }



        public ActionResult HourReport(int id)
        {
            var day = Convert.ToDateTime(Request.QueryString["d"]);

            var allTickets = db.Tickets.Where(x=> x.DateClosed != null).ToList()
                .Where(x => x.TicketTypeID == 1 && x.MachineID == id &&
            x.DateOpen.Date == day.Date).ToList();


            //  List<TicketEvent> te = new List<TicketEvent>();
            _app.PagesModel.HourReportPage hr = new _app.PagesModel.HourReportPage();
            hr.TicketEvents = (from x in allTickets
                  select new _app.PagesModel.TicketEvent
                  {
                      backgroundColor = x.Machien.dbServer.Branch.rgba,
                      end = x.DateClosed.Value,
                      start = x.DateOpen,
                      title = x.Machien.dbServer.Branch.Name + " for " + _app.UI.TimeSpanToString(x.DateClosed.Value - x.DateOpen)

                  }).ToList();
            hr.Day = day;
            return View(hr);
        }
    }
}