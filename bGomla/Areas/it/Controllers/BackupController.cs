﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _app.PagesModel;
using bGomlaDB;

namespace bGomla.Areas.it.Controllers
{
    public class BackupController : Controller
    {

        private bGomlaDBEntities db = new bGomlaDBEntities();
        public ActionResult ServerBackup(int id)
        {

            var srv = db.dbServers.Find(id);
            List<_app.PagesModel.BackupPlan> plans = new List<_app.PagesModel.BackupPlan>();
            _app.Files file = new _app.Files();
            _app.WebAPI.Response response = new _app.WebAPI.Response();
            if (srv.Online)
            {
                response.Status = 1;
                foreach (var p in srv.Backups)
                {
                    var plan = new BackupPlan { PlaneName = p.BackupType.Name , interval = p.MinuteInterval ,
                        extention = p.BackupType.Extention, BackupTypeID = p.BackupTypeID };

                   // plan.PlaneName = p.BackupType.Name;

                    foreach (var loc in p.BackupLocations)
                    {

                        var allfiles = file.FilesInFolder(loc.Root, p.BackupType.Extention ).OrderByDescending(x=> x.CreationTime);

                        if(allfiles.Count() > 0) {
                            plan.BackupLocations.Add(new BackupLocations { Root = loc.Root,
                                LastBackup = allfiles.FirstOrDefault().LastWriteTime, FristBackup = allfiles.LastOrDefault().LastWriteTime,
                                Name = loc.Name, FilesCount = allfiles.Count(),
                                MinuteInterval = loc.Backup.MinuteInterval, RootSize = allfiles.ToList().Sum(x => x.Length)  });


                            if ( plan.BackupTypeID == 1 && plan.BackupLocations.All(x=> x.AverageFileGrowth == 0 ) )
                            {
                                FileInfo oldfile, newfile;
                                int i;
                                foreach (var f in allfiles)
                                {
                                    oldfile = f;
                                    i = allfiles.ToList().IndexOf(f);
                                    if ((i+1) < allfiles.Count())
                                    {
                                    newfile = allfiles.ElementAt(i + 1);
                                    if(oldfile != null && newfile != null)
                                    { plan.BackupLocations.LastOrDefault().AverageFileGrowth += (oldfile.Length - newfile.Length); }

                                    }
                                
                                }
                                plan.BackupLocations.LastOrDefault().AverageFileGrowth = (plan.BackupLocations.LastOrDefault().AverageFileGrowth / allfiles.Count());
                            }
                           


                        }
                        else
                        {
                            plan.BackupLocations.Add(new BackupLocations { Root = loc.Root, Name = loc.Name + " location is offline", FilesCount = 0 ,
                                MinuteInterval = loc.Backup.MinuteInterval , RootSize  = 0 , AverageFileGrowth = 0   });
                            response.Status = 0;

                        }
                       
                    }

                    plans.Add(plan);
                }


            }


            response.Data = ConvertPartialViewToString(PartialView("~/Areas/it/Views/Shared/Backup/_Plans.cshtml", plans));

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {

            var model = (from x in db.dbServers select new _app.PagesModel.BackupPartial
            { BackupID = x.ServerID , Branch = x.Branch.Name }).ToList();

            // return View(db.dbServers.ToList());
            return View(model);
        }

        public ActionResult BranchServer(int id)
        {
            _app.PagesModel.BackupPartial model = new BackupPartial { BackupID = id , Branch = db.dbServers
            .Find(id).Branch.Name};
            return View(model);
        }

        protected string ConvertPartialViewToString(PartialViewResult partialView)
        {
            using (var sw = new StringWriter())
            {
                partialView.View = ViewEngines.Engines
                  .FindPartialView(ControllerContext, partialView.ViewName).View;

                var vc = new ViewContext(
                  ControllerContext, partialView.View, partialView.ViewData, partialView.TempData, sw);
                partialView.View.Render(vc, sw);

                var partialViewString = sw.GetStringBuilder().ToString();

                return partialViewString;
            }
        }
    }
}