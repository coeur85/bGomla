﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using bGomlaDB;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.Routing;

namespace bGomla.Areas.Sales.Controllers
{
    public class HomeController : Controller
    {
        // GET: Sales/Home
        bGomlaDBEntities db = new bGomlaDBEntities();




        private _Retail.PageModel.Sales.SalesHome DailySalesData()
        {
            _Retail.Sales sales = new _Retail.Sales();

            return sales.DailySalesData();

        }



        public ActionResult Index()
        {

            return View(DailySalesData());
        }



        public static string RenderPartialViewToString(string viewName, object model)
        {
            using (var sw = new StringWriter())
            {
                bGomla.Areas.Sales.Controllers.HomeController controller = new bGomla.Areas.Sales.Controllers.HomeController();
                // instance of the required controller (you can pass this as a argument if needed)

                // Create an MVC Controller Context
                var wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);

                RouteData routeData = new RouteData();

                routeData.Values.Add("controller", controller.GetType().Name
                                                            .ToLower()
                                                            .Replace("controller", ""));

                controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);

                controller.ViewData.Model = model;

                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.ToString();
            }
        }




        //public ActionResult SendMail()
        //{

        //    var data = DailySalesData();
        //    //  var view = RenderRazorViewToString("report", DailySalesData());
        //    var view = RenderPartialViewToString("~/Areas/Sales/Views/Home/report.cshtml",data);
        //    _app.Send.eMail mail = new _app.Send.eMail();
        //     mail.SalesDailyReport(new MailAddress("ahmed.magdi@bgomla.com"), view );
        //    return View();
        //}


       

    }
}