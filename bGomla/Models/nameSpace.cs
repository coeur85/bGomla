﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using bGomlaDB;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Threading;

namespace _app
{
    namespace PagesModel
    {


        public class Subscription
        {
            public dbServer Publiser { get; set; }
            public dbServer Subscriber { get; set; }

            public List<Artical> Articals { get; set; }
            public Publication Publication { get; set; }

            public Subscription() { Articals = new List<Artical>(); }
            public Subscrtion Subscrtion { get; set; }

        }
        public class SubscriptionFailed
        {

            public bGomlaDB.Subscrtion subscription { get; set; }
            public string errorMessage { get; set; }

        }
        public class Artical {
            public string ArticalName { get; set; }
            public int? PublisherCount { get; set; }
            public int? SubScriperCount { get; set; }
            public int ArticalID { get; set; }
            public int SubID { get; set; }
            public string GroupBy { get; set; }

        }
        public class SubPartial
        {
            public int SubID { get; set; }
            public string PublicationName { get; set; }
            public string PublisherBranch { get; set; }
            public string SubscriperBranch { get; set; }

        }
        public class BackupPartial
        {
            public string Branch { get; set; }
            public int BackupID { get; set; }

        }
        public class GroupedData {

            public List<WebAPI.GroupedData> GroupedDatas { get; set; }
            public string NewFilter { get; set; }
            public int SubID { get; set; }
            public GroupedData() { GroupedDatas = new List<WebAPI.GroupedData>(); }
        }
        public class BackupPlan
        {
            public string PlaneName { get; set; }
            public int interval { get; set; }
            public string extention { get; set; }
            public int BackupTypeID { get; set; }
            public List<BackupLocations> BackupLocations { get; set; }


            public BackupPlan() { BackupLocations = new List<BackupLocations>(); }

        }
        public class BackupLocations {
            public string Name { get; set; }
            public string Root { get; set; }
            public DateTime LastBackup { get; set; }
            public DateTime FristBackup { get; set; }
            public int FilesCount { get; set; }
            public int MinuteInterval { get; set; }
            public long AverageFileGrowth { get; set; }
            public long RootSize { get; set; }

        }
        public class TwoCoulmsPage{
            public string PartialPath { get; set; }
            public object SourceData { get; set; }

            public List<object> List1  { get; set; }
            public List<object> List2 { get; set; }


            public TwoCoulmsPage() { SourceData = new List<object>(); List1 = new List<object>(); List2 = new List<object>(); }
        }
        public class TicketNotificationList {
            public string icon { get; set; }
            public TicketType TicketType { get; set; }
            public List<Ticket> Tickets { get; set; }
            public string CssClassName { get; set; }
            public TicketNotificationList() { Tickets = new List<Ticket>(); }
        }
        public class TicketTypeHomePage {
            public ChartClass Chart1 { get; set; }
            public string TypeName { get; set; }
            public DateTime From { get; set; }
            public DateTime TO { get; set; }
            public List<onlineServers> OnlineServers { get; set; }
            public OfflineServersTable offlineServersTable { get; set; }
            public TicketTypeHomePage() { OnlineServers = new List<onlineServers>();  }
        }
        public class onlineServers
        {
            public string name { get; set; }
            public bool online { get; set; }
        }
        public class OfflineServersTable
        {
            public List<dbServer> dbServers { get; set; }
            public List<OfflineServerDayPeroid> offlineServerDayPeroids { get; set; }

            public OfflineServersTable() { offlineServerDayPeroids = new List<OfflineServerDayPeroid>(); }
        }
        public class OfflineServerDayPeroid
        {
            public DateTime Date { get; set; }
            public List<TimeSpan> Peroid { get; set; }

            public OfflineServerDayPeroid() { Peroid = new List<TimeSpan>(); }
        }
        public class ChartClass
        {
            public string[] Lables { get; set; }
           
            public string text { get; set; }
            public string PointTilite { get; set; }
           
            public string xAexTitle { get; set; }
            public string yAexTitle { get; set; }
            public int CanvesID { get; set; }
            // public ChartClass() { Lables = new List<object>(); Data = new List<object>(); }
            public string ChartType { get; set; }

            public List<ChartDataSet> ChartDataSets { get; set; }

            public ChartClass() { ChartDataSets = new List<ChartDataSet>(); }

        }
        public class ChartDataSet {
            public string Color { get; set; }
            public string[] Data { get; set; }
            public string label { get; set; }
            public string[] Colors { get; set; }
        }
        public class PreformanceBar {

            public long TotalValue { get; set; }
            public long UsedAmount { get; set; }
            public string FreeTilte { get; set; }
            public string UsedTitle { get; set; }
            public string BarTitle { get; set; }

        }
        public class DriveSpaceBar
        {

            public long Capacity { get; set; }
            public long FreeSpace { get; set; }
            public long DatabaseSize { get; set; }
            public int ServerID { get; set; }

        }

        public class HourReportPage
        {
            public List<TicketEvent> TicketEvents { get; set; }
            public DateTime Day { get; set; }


            public HourReportPage() { TicketEvents = new List<TicketEvent>(); }
        }



        public class TicketEvent
        {

            public string title { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }
            public bool allDay { get { return false; } }
            public string backgroundColor { get; set; }
        }
    }
    namespace WebAPI
    {
        public class Response
        {


            public int Status { get; set; }
            public object Data { get; set; }

        }
        public class OfflineServers
        {
            public dbServer Publiser { get; set; }
            public dbServer Subscriper { get; set; }

        }
        public class GroupedData
        {
            public DateTime RowDate { get; set; }
            public int Count { get; set; }

        }

        
    }
    class ADO
    {

        //private SqlConnection SqlConnection;
        //private SqlConnection Connection(string connction) {
        //    if (SqlConnection == null) { SqlConnection = new SqlConnection(connction); }
        //    return SqlConnection;
        //}

       

        public static string SqlDateformate(DateTime date)
        {
            return "'" + date.Year.ToString() + "-"
                    + date.Month.ToString() + "-" + date.Day.ToString() + "'";
        }
        public static string Filter(string fil, Branch br) {
            if (!string.IsNullOrEmpty(fil))
            {
                string where = " where " + fil;
                where = where.Replace("{branchCode}", br.Code.ToString());
                return where;
            }
            return null;
        }
        private int ExecuteScalar(string connction, string commanText)
        {
            SqlCommand command = new SqlCommand(commanText, new SqlConnection(connction) ); 
            try
            {
                if (command.Connection.State != System.Data.ConnectionState.Open)
                { command.Connection.Open(); }
                return Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                command.Connection.Close();

            }
        }
        public int? RowCount(string articalName , string filter, string connticon, Branch br)
        {
            string s = "select count(*) from " + articalName;

            s = s + Filter(filter, br);
            try
            {
                return ExecuteScalar(connticon, s);
            }
            catch (Exception)
            {

                return null;
            }
            

        }       
        public DataTable DataTable(string connection , string  commanText)
        {
            SqlCommand command = new SqlCommand(commanText, new SqlConnection(connection) );
            try
            {
                if (command.Connection.State != System.Data.ConnectionState.Open)
                { command.Connection.Open(); }

              //  command.CommandText = "set FMTONLY on " + command.CommandText + " set FMTONLY off";

                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable t = new DataTable();


               
                for (int attempts = 0; attempts <= 50; attempts++)
                // if you really want to keep going until it works, use   for(;;)
                {
                    try
                    {
                        da.Fill(t);
                        return t;
                    }
                    catch { }
                    Thread.Sleep(1000); // Possibly a good idea to pause here, explanation below
                }

                throw new Exception("too many attepms !");

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                // command.Connection.Close();
                command.Dispose();
                command = null;
                GC.Collect();
            }


        }    
        public List<WebAPI.GroupedData> GroupedDatas(Branch publisherBranch , Artical art, string databaseConnection ) {

            string s = "select " + art.GroupBy + " , count(*) as [count] from " + art.ArticalName;
            s += Filter(art.Fillter, publisherBranch);
            s += (" group by " + art.GroupBy);
            var dt = DataTable(databaseConnection, s);

            List<WebAPI.GroupedData> gd = new List<WebAPI.GroupedData>();

            gd = dt.AsEnumerable().Select(x => new WebAPI.GroupedData { RowDate = x.Field<DateTime>(0),
                Count = x.Field<int>(1) }).ToList();

            return gd;
        }
        public long DatabaseSize(dbServer srv) {

            var t = DataTable(srv.ConnectionString, "exec sp_spaceused");

            var value = t.Rows[0].Field<string>(1);
            value = value.Replace(" MB", "");
            decimal mb = Convert.ToDecimal(value);
            return Convert.ToInt64( mb * 1048576);

        }
    }
    public class UI {
        private static bGomlaDBEntities db = new bGomlaDBEntities();
        public static List<Branch> Branches() {
            return db.Branches.ToList();
        }
        public static List<dbServer> dbServers()
        {
            return db.dbServers.ToList();
        }
        public static List<Subscrtion> Subscrtions(int publihserID , int subscriperid)
        {
            return db.Subscrtions.Where(x => x.SubscriperServerID == subscriperid && x.ServerPublication.ServerID == publihserID).ToList();
        }
        public static List<PublicationCategory> publicationCategories() {
            // return db.PublicationCategories.Where(x=> x.CatID != 1).ToList();
            return db.PublicationCategories.ToList();
        }
        public static string RenderPartialToString(string controlName, object viewData)
        {
            ViewPage viewPage = new ViewPage() { ViewContext = new ViewContext() };

            viewPage.ViewData = new ViewDataDictionary(viewData);
            viewPage.Controls.Add(viewPage.LoadControl(controlName));

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            {
                using (HtmlTextWriter tw = new HtmlTextWriter(sw))
                {
                    viewPage.RenderControl(tw);
                }
            }

            return sb.ToString();
        }
        public static _app.PagesModel.SubPartial SubClass(int id)
        {
            var sub = db.Subscrtions.Find(id);
           return SubClass(sub);
        }
        public static _app.PagesModel.SubPartial SubClass(Subscrtion subscrtion)
        {
            return new PagesModel.SubPartial
            {
                PublicationName = subscrtion.ServerPublication.Publication.Name,
                PublisherBranch = subscrtion.ServerPublication.dbServer.Branch.Name,
                SubscriperBranch = subscrtion.SubscriptionServer.Branch.Name,
                SubID = subscrtion.SubscriptionID
            };

        }
        public static _app.PagesModel.BackupPartial BackupClass(int id)
        {
            var sr = db.dbServers.Find(id);
            return BackupClass(sr);
        }
        public static _app.PagesModel.BackupPartial BackupClass(dbServer server)
        {

            return new PagesModel.BackupPartial { BackupID = server.ServerID,
                Branch = server.Branch.Name };
        }
        public static PagesModel.TwoCoulmsPage TwoCoulmsPage(PagesModel.TwoCoulmsPage model)
        {

            model.List1.Clear();
            model.List2.Clear();

            var list = ((IEnumerable)model.SourceData).Cast<object>().ToList();
            for (int i = 0; i < list.Count; i++)
            {

                if (i % 2 == 0) { model.List1.Add(list.ElementAt(i)); }
                else { model.List2.Add(list.ElementAt(i)); }

            }

            return model;

        }
        public class Notifications {

            public static List<Ticket> Tickets(int typeid) {

                return db.Tickets.Where(x => x.StatusID == TicketStatus.Open && x.TicketTypeID == typeid).ToList();
            }

        }

        public static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
        public static string TimeSpanToString(TimeSpan t)
        {
            if (t.TotalSeconds <= 1)
            {
                return $@"{t:s\.ff} seconds";
            }
            if (t.TotalMinutes <= 1)
            {
                return $@"{t:%s} seconds";
            }
            if (t.TotalHours <= 1)
            {
                return $@"{t:%m} minutes";
            }
            if (t.TotalDays <= 1)
            {
                return $@"{t:%h} hours";
            }

            return $@"{t:%d} days";
        }

        public static string expectedFileSize(long avrage, long rootsize) {


            if (rootsize < 10737418240) // 10 GB
            {

                var dif = 10737418240 - rootsize;
                double days = (dif / avrage);
                return BytesToString(10737418240) + " in " + Math.Round(days, 0).ToString() + " days";
            }

            else if (rootsize < 32212254720) // 30 GB
            {

                var dif = 32212254720 - rootsize;
                double days = (dif / avrage);
                return BytesToString(32212254720) + " in " + Math.Round(days, 0).ToString() + " days";
            }

            else if (rootsize < 53687091200) // 50 GB
            {

                var dif = 53687091200 - rootsize;
                double days = (dif / avrage);
                return BytesToString(53687091200) + " in " + Math.Round(days, 0).ToString() + " days";
            }


            else if(rootsize < 107374182400) // 100 GB
            {

                var dif = 107374182400 - rootsize;
                double days = (dif / avrage);
                return BytesToString(107374182400) + " in " + Math.Round(days, 0).ToString() + " days";
            }

            else if (rootsize < 214748364800) // 200 GB
            {

                var dif = 214748364800 - rootsize;
                double days = (dif / avrage);
                return BytesToString(214748364800) + " in " + Math.Round(days, 0).ToString() + " days";
            }
            //

            return null;

        }


        
    }  
    public class Files {

        public FileInfo[] FilesInFolder(string folderPath, string fileExtention)
        {
            DirectoryInfo d = new DirectoryInfo(folderPath);
            try
            {
                    return d.GetFiles("*" + fileExtention);
            }
            catch (Exception)
            {

                //throw;
                return  new FileInfo[0];
            }
           
        }
    }
}

