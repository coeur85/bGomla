﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using bGomlaDB;
using retailClass;

namespace _Retail
{
    namespace PageModel {
        namespace Sales {

            public class SalesHome {
                public _app.PagesModel.ChartClass Chart { get; set; }
                public List<BranchSalesDate> BranchSalesDate { get; set; }

                public SalesHome() { BranchSalesDate = new List<BranchSalesDate>(); }
            }

            public class BranchSalesDate {
                public Branch Branch { get; set; }
                public DateTime Date { get; set; }
            }

        }

    }
    public class Sales
    {

        bGomlaDBEntities db;
        private  List<RetailReport> salesReport { get
            {
                return db.RetailReports.Where(x => x.ReportTypeID == 1).ToList();
            }

        }
        _app.ADO ado = new _app.ADO();




        private List<retailClass.DailyReport> DailyReport(ICollection<Branch> bc, DateTime from, DateTime to)
        {

            db = new bGomlaDBEntities();
            var sr = salesReport.Where(x => x.ReportID == 1).FirstOrDefault();
            QueryFilter filter = new QueryFilter(sr.SqlQuery);

            sr.SqlQuery = filter.Branch(bc);
            sr.SqlQuery = filter.DateFrom(from);
            sr.SqlQuery = filter.DateTo(to);
            dbServer hq = db.dbServers.Find(1);
            var dt = ado.DataTable(hq.ConnectionString, sr.SqlQuery);

            List<retailClass.DataTables.DailyReport> dtdr =
                dt.AsEnumerable().Select(x => new retailClass.DataTables.DailyReport {
                     branch = x.Field<int>(0),
                     doctype = x.Field<int>(1),
                     invoicesDate = x.Field<DateTime>(2),
                     cost = x.Field<decimal>(3),
                     total = x.Field<decimal>(4),
                     totalDiscount = x.Field<decimal>(5)
                }).ToList();

            List<retailClass.DailyReport> saleslist = new List<retailClass.DailyReport>();

            foreach (var b in bc)
            {
                for (DateTime i = from; i <= to; i = i.AddDays(1))
                {

                    var brecord = dtdr.Where(x => x.invoicesDate.Date == i.Date && x.branch == b.Code).ToList();

                    //var item99 = brecord.Where(x => x.doctype == 99).FirstOrDefault();
                    decimal value99 = Item99(brecord);
                    //if (item99 == null) { value99 = 0; }
                    //else { value99 = item99.totalDiscount; }

                    decimal totalValue = Total(brecord);
                    //   brecord.Where(x=> 
                    //x.doctype == 2080).FirstOrDefault().total;
                    decimal totalDiscount = Discount(brecord);
                    //brecord.Where(x => x.doctype == 2080)
                    //.FirstOrDefault().totalDiscount + brecord.Where(x => x.doctype == 2030)
                    //.FirstOrDefault().totalDiscount + value99;
                    decimal totalRreturns = Returns(brecord);
                    //  brecord.Where(x => x.doctype == 2030).FirstOrDefault().total;
                    //if(value99 == 0 && totalValue ==0 && totalDiscount == 0 && totalRreturns == 0)
                    //{ break; }

                    if (totalRreturns < 0) { totalRreturns = totalRreturns * -1; }
                    if (totalDiscount < 0) { totalDiscount = totalDiscount * -1; }
                    saleslist.Add(new retailClass.DailyReport
                    {
                        Branch = b,
                        Date = i.Date,
                        // Total = (totalValue - totalDiscount) - totalRreturns 
                        totalSales = totalValue,
                        totalDiscount = totalDiscount,
                        totalReturns = totalRreturns,
                        Total = (totalValue - totalDiscount) - totalRreturns

                    });

                }
            }

          

            return saleslist;

        }

        private decimal Item99(List<retailClass.DataTables.DailyReport> brecord)
        {
            var dailyReport = brecord.Where(x => x.doctype == 99).FirstOrDefault();

            if (dailyReport == null) { return 0; }
            else { return dailyReport.totalDiscount; }

        }
        private decimal Total(List<retailClass.DataTables.DailyReport> brecord)
        {
            var dailyReport = brecord.Where(x => x.doctype == 2080).FirstOrDefault();

            if (dailyReport == null) { return 0; }
            else { return dailyReport.total; }

        }
        private decimal Discount(List<retailClass.DataTables.DailyReport> brecord)
        {
            var it2080 = brecord.Where(x => x.doctype == 2080)
                      .FirstOrDefault();
            var it2030 = brecord.Where(x => x.doctype == 2030)
                     .FirstOrDefault();
            var it99 = Item99(brecord);

            if (it2030 != null && it2080 != null) { return (it2080.totalDiscount + it2030.totalDiscount + it99); }
            else { return 0; }
        }
        private decimal Returns(List<retailClass.DataTables.DailyReport> brecord)
        {
            var dailyReport = brecord.Where(x => x.doctype == 2030).FirstOrDefault();

            if (dailyReport == null) { return 0; }
            else { return dailyReport.total; }

        }



        public _Retail.PageModel.Sales.SalesHome DailySalesData()
        {

            _Retail.Sales sales = new _Retail.Sales();

            DateTime from = DateTime.Today.AddDays(-7);
            DateTime to = DateTime.Today;
            _Retail.PageModel.Sales.SalesHome sh = new _Retail.PageModel.Sales.SalesHome();
            List<retailClass.DailyReport> list = new List<retailClass.DailyReport>();
            db = new bGomlaDBEntities();
            var bl = db.Branches.ToList()
                .Where(x => x.dbServers.Any(s => s.ServerPublications.Any(p => p.Publication.PublicationCategory.CatID == 2))).ToList();
            //foreach (var br in bl)
            //{
            //    list.AddRange(sales.DailyReport(br, from, to));
            //}
            list.AddRange(sales.DailyReport(bl, from, to));
            sh.Chart = new _app.PagesModel.ChartClass
            {
                CanvesID = 1,
                ChartType = "line",
                PointTilite = "Sales",
                xAexTitle = "Dates",
                text = "Sales Chart",
                yAexTitle = "Amount",
                Lables = list.GroupBy(x => x.Date).Where(x => x.Key.Date.Date < DateTime.Today.Date).Select(x => x.Key.Date.ToShortDateString()).ToArray()
            };
            //   sh.BranchSalesDate = new List<_Retail.PageModel.Sales.BranchSalesDate>();
            foreach (var br in bl)
            {


                if (list.Where(y => y.Branch.BranchID == br.BranchID && y.Total > 0).Count() > 0)
                {
                    DateTime maxDate = list.Where(y => y.Branch.BranchID == br.BranchID && y.Total > 0).Max(y => y.Date).AddDays(-1).Date;

                    sh.Chart.ChartDataSets.Add(new _app.PagesModel.ChartDataSet
                    {
                        Color = br.rgba,
                        label = br.Name,
                        Data = list.Where(x => x.Branch.BranchID == br.BranchID
                        && x.Date.Date <= maxDate
                     //   && x.Total > 0
                     )
                        .Select(x => Math.Round(x.Total, 0).ToString()).ToArray()
                    });

                    sh.BranchSalesDate.Add(new _Retail.PageModel.Sales.BranchSalesDate
                    {
                        Branch = br,
                        Date = list.Where(x => x.Branch.BranchID == br.BranchID
                        && x.Total > 0
                       )
                        .Max(x => x.Date)

                    });

                }
                else // new branch no sales at all
                {

                    sh.Chart.ChartDataSets.Add(new _app.PagesModel.ChartDataSet
                    {
                        Color = br.rgba,
                        label = br.Name,
                        Data = list.Where(x => x.Branch.BranchID == br.BranchID
                        && x.Date.Date <= DateTime.Today.AddDays(-1)).Select(x => Math.Round(x.Total, 0).ToString()).ToArray()
                    });

                    sh.BranchSalesDate.Add(new _Retail.PageModel.Sales.BranchSalesDate
                    {
                        Branch = br,
                        Date = list.Where(x => x.Branch.BranchID == br.BranchID).Max(x => x.Date)

                    });


                }





            }
            return sh;

        }

    }
    class QueryFilter {

        public QueryFilter(string value)
        {
            Query = value;
        }
        private string Query { get; set; }

        public  string Branch(ICollection<Branch> bc)
        {

            string s = "";
            foreach (var b in bc)
            {
                s += b.Code.ToString() + ",";
            }

            s = s.Remove(s.Length - 1);
            s = "(" + s + ")";
             Query  =Query.Replace("{branchCode}", s);
            return Query;
        }
        public string DateFrom(DateTime date)
        {
            Query = Query.Replace("{dateFrom}", _app.ADO.SqlDateformate(date));
            return Query;
        }
        public string DateTo(DateTime date)
        {
            Query = Query.Replace("{dateTo}", _app.ADO.SqlDateformate(date));
            return Query;
        }


    }

}

namespace retailClass
{

    namespace DataTables {
        public class DailyReport
    {
        public int branch { get; set; }
        public int doctype { get; set; }
        public DateTime invoicesDate { get; set; }
        public decimal cost { get; set; }
        public decimal total { get; set; }
        public decimal totalDiscount { get; set; }
       

        }
    }

    public class DailyReport {

        public Branch Branch { get; set; }
        public  DateTime Date { get; set; }
        public decimal Total { get; set; }
        public decimal totalSales { get; set; }
        public decimal totalDiscount { get; set; }
        public decimal totalReturns { get; set; }
    }

}