﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using bGomlaDB;

namespace bGomla.Controllers.API
{
    public class BackupController : ApiController
    {
        bGomlaDBEntities db = new bGomlaDBEntities();
        [HttpGet]
        public IHttpActionResult Full() {
            CheckBackup(1, 2);
            return Ok("Full compelted");
        }
        [HttpGet]
        public IHttpActionResult Differential()
        {
            CheckBackup(2, 3);
            return Ok("Differential compelted");
        }
        [HttpGet]
        public IHttpActionResult Transaction()
        {
            CheckBackup(3, 5);
            return Ok("Transaction compelted");
        }

        private void CheckBackup(int backuptypeID, int TicketTypeID)
        {

            var allPlans = db.Backups.Where(x => x.BackupTypeID == backuptypeID).ToList();
            _app.Files files = new _app.Files();



            foreach (var p in allPlans)
            {
                if (p.dbServer.Online)
                {

                    foreach (var loc in p.BackupLocations)
                    {

                        var lastfile = files.FilesInFolder(loc.Root, p.BackupType.Extention).OrderByDescending(x => x.CreationTime).FirstOrDefault();
                        var t = db.Tickets.Where(x => x.MachineID == p.dbServerID && x.TicketTypeID
                        == TicketTypeID && x.StatusID != TicketStatus.Closed && x.BackupLocationID == loc.LocationID).FirstOrDefault();

                        if (lastfile != null)
                        {
                            if (lastfile.CreationTime.AddMinutes(p.MinuteInterval) >= DateTime.Now)
                            {

                                if (t != null)
                                {
                                    t.DateClosed = DateTime.Now;
                                    t.StatusID = TicketStatus.Closed;
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                OpenTicket(t, TicketTypeID, loc);
                            }

                        }




                        else
                        {
                            OpenTicket(t, TicketTypeID, loc);

                            //if (t == null)
                            //{
                            //    t = new Ticket
                            //    {
                            //        Counter = 0,
                            //        DateOpen = DateTime.Now,
                            //        MachineID = p.dbServerID,
                            //        StatusID = TicketStatus.Underinvestegation,
                            //        TicketTypeID = TicketTypeID
                            //    };

                            //    db.Tickets.Add(t);
                            //}
                            //t.Counter += 1;
                            //var allwoed = db.TicketTypes.Find(TicketTypeID).AllowedCounter;
                            //if (t.Counter >= allwoed)
                            //{
                            //    t.StatusID = TicketStatus.Open;
                            //}
                            //db.SaveChanges();
                        }
                    }
                }

            }

        }

        private void OpenTicket(Ticket t, int TicketTypeID, BackupLocation location) {

            if (t == null)
            {
                t = new Ticket
                {
                    Counter = 0,
                    DateOpen = DateTime.Now,
                    MachineID = location.Backup.dbServerID,
                    StatusID = TicketStatus.Underinvestegation,
                    TicketTypeID = TicketTypeID,
                    BackupLocationID = location.LocationID

                };

                db.Tickets.Add(t);
            }
            t.Counter += 1;
            var allwoed = db.TicketTypes.Find(TicketTypeID).AllowedCounter;
            if (t.Counter >= allwoed)
            {
                t.StatusID = TicketStatus.Open;
            }
            db.SaveChanges();

        }
    }
}
