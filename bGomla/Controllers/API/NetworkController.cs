﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using bGomlaDB;

namespace bGomla.Controllers.API
{
    public class NetworkController : ApiController
    {

        bGomlaDBEntities db = new bGomlaDBEntities();

        [HttpGet]
        public IHttpActionResult Servers()
        {
           
            var sl = db.dbServers.ToArray();

            string s = "";




            foreach (var srv in sl)
            {

                var t = db.Tickets.FirstOrDefault(x => x.StatusID != TicketStatus.Closed && x.MachineID == srv.ServerID
                        && x.TicketTypeID == 1);
                for (int i = 0; i < 2; i++)
                {

                    System.Net.NetworkInformation.Ping p = new System.Net.NetworkInformation.Ping();

                    if (p.Send(srv.Machien.IP).Status == System.Net.NetworkInformation.IPStatus.Success) {

                        // close any open tickets
                        if (t != null) { t.DateClosed = DateTime.Now; t.StatusID = TicketStatus.Closed; db.SaveChanges();}
                        break;
                    };


                    if (i == 1)
                    {
                        // s += srv.Branch.Name + " is bad.   ";
                        // open or update server ticket
                        var allowd = db.TicketTypes.Find(1).AllowedCounter;
                        if (t == null) {
                            t = new Ticket { Counter = 0 , DateOpen = DateTime.Now , MachineID = srv.ServerID ,
                                StatusID = TicketStatus.Underinvestegation,
                                TicketTypeID = 1  };
                            db.Tickets.Add(t);

                        }
                        t.Counter += 1;
                        if (t.Counter >= allowd )
                        { t.StatusID = TicketStatus.Open;  }

                       
                         db.SaveChanges();
                        
                       
                    }
                }
                
            }

         
            return Ok(s);
        }

    }
}
