﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace bGomla.Controllers.API
{
    public class SalesController : ApiController
    {


        private static string RenderPartialViewToString(string viewName, object model)
        {
            using (var sw = new StringWriter())
            {
                bGomla.Areas.Sales.Controllers.HomeController controller = new bGomla.Areas.Sales.Controllers.HomeController();
                // instance of the required controller (you can pass this as a argument if needed)

                // Create an MVC Controller Context
                var wrapper = new HttpContextWrapper(System.Web.HttpContext.Current);

                RouteData routeData = new RouteData();

                routeData.Values.Add("controller", controller.GetType().Name
                                                            .ToLower()
                                                            .Replace("controller", ""));

                controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);

                controller.ViewData.Model = model;

                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);

                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.ToString();
            }
        }
        


        [System.Web.Http.HttpGet]
        public IHttpActionResult SendDailyReport()
        {


            try
            {

                _Retail.Sales sales = new _Retail.Sales();
                var data = sales.DailySalesData();
                //  var view = RenderRazorViewToString("report", DailySalesData());
                var view = RenderPartialViewToString("~/Areas/Sales/Views/Home/report.cshtml", data);
                _app.Send.eMail mail = new _app.Send.eMail();
                mail.SalesDailyReport("Daily Sales " +
                    DateTime.Today.Day + "/" + DateTime.Today.Month, view);
                return Ok("sent");

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

        }

    }
}
