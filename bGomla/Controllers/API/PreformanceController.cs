﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using bGomlaDB;
using System.Management;

namespace bGomla.Controllers.API
{
    public class PreformanceController : ApiController
    {

        bGomlaDBEntities db = new bGomlaDBEntities();
        [HttpGet]
        public IHttpActionResult Servers()
        {

           // Log = "in method";
            try
            {
                foreach (var srv in db.dbServers.ToList())
                {
                    if (srv.Online)
                    {
                       // Log = srv.Branch.Name + "is working";
                        PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", srv.Machien.Name);
                        PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available Bytes", String.Empty, srv.Machien.Name);
                        cpuCounter.NextValue();
                        System.Threading.Thread.Sleep(1000);
                      //  Log = "loaded ram + cpu";

                        var flot1 = cpuCounter.NextValue();
                      //  Log = "loaded cpu";
                        var flot2 = ramCounter.NextValue();
                      //  Log = "loaded ram";
                        MachinePerformanceCounter pc = new MachinePerformanceCounter
                        {
                            CPU = cpuCounter.NextValue(),
                            Ram = ramCounter.NextValue(),
                            MachineID = srv.ServerID,
                            pcDate = DateTime.Now
                        };
                        //  Log = "added ram + cpu";
                        ManagementPath path = new ManagementPath()
                        {
                            NamespacePath = @"root\cimv2",
                            Server = srv.Machien.Name
                        };

                        //ManagementPath path = new ManagementPath();
                        //Log = "new path object created";
                        //path.NamespacePath = @"root\cimv2";//@"\\.\root\cimv2";
                        ////@"root\cimv2";
                        //Log = "added namespace";
                        //path.Server = srv.Machien.Name;
                        //Log = "added server name";
                        ManagementScope scope = new ManagementScope(path);
                       // Log = "added scope";
                        string condition = "DriveLetter = '" + srv.DriveLetter.ToUpper() + ":'";
                        string[] selectedProperties = new string[] { "FreeSpace", "Capacity" };
                        SelectQuery query = new SelectQuery("Win32_Volume", condition, selectedProperties);
                        long free = 0, Capacity = 0;
                       // Log = "got HD";
                        using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
                        using (ManagementObjectCollection results = searcher.Get())
                        {
                            ManagementObject volume = results.Cast<ManagementObject>().FirstOrDefault();

                            if (volume != null)
                            {
                                ulong freeSpace = (ulong)volume.GetPropertyValue("FreeSpace");
                                ulong capacityTotal = (ulong)volume.GetPropertyValue("Capacity");
                                free = Convert.ToInt64(freeSpace);
                                Capacity = Convert.ToInt64(capacityTotal);
                                srv.DriveFreeSpace = free;
                                srv.DriveCapacity = Capacity;
                            }
                            _app.ADO aDO = new _app.ADO();
                            srv.DatabaseSize = aDO.DatabaseSize(srv);
                            db.MachinePerformanceCounters.Add(pc);
                            db.SaveChanges();
                           // Log = "saved";
                        }

                        if (srv.LocalBackupLocationsDiskSpace != null)
                        {

                            var local = srv.LocalBackupLocationsDiskSpace;
                            condition = "DriveLetter = '" + local.DriveLetter.ToUpper() + ":'";
                             selectedProperties = new string[] { "FreeSpace", "Capacity" };
                             query = new SelectQuery("Win32_Volume", condition, selectedProperties);
                            
                            // Log = "got HD";
                            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
                            using (ManagementObjectCollection results = searcher.Get())
                            {
                                ManagementObject volume = results.Cast<ManagementObject>().FirstOrDefault();

                                if (volume != null)
                                {
                                    ulong freeSpace = (ulong)volume.GetPropertyValue("FreeSpace");
                                    ulong capacityTotal = (ulong)volume.GetPropertyValue("Capacity");
                                    free = Convert.ToInt64(freeSpace);
                                    Capacity = Convert.ToInt64(capacityTotal);
                                    local.DriveFreeSpace = free;
                                    local.DriveCapacity = Capacity;
                                }
                                db.SaveChanges();
                                
                            }

                        }



                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                // return Ok(Log + "---" + e.Message);
                return InternalServerError(e);
              
            }

           

        }

        //private string _log = "";
        //private string Log { get { return _log; } set { _log += (Environment.NewLine + value); } }
    }
}
