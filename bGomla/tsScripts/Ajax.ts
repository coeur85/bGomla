﻿module AjaxHelper {
    

    export class Subscription {
        className: string;

        constructor() {
            this.className = '';
        }

        private callWebApi(url: string,data:any,
             callback): void {

            let xhr = new XMLHttpRequest();

            xhr.onload = function () {
                let data = JSON.parse(xhr.responseText);
                callback(data);
            }

            xhr.onerror = function () {
                alert("Error while calling Web API");
            }

            

            xhr.open('POST', url);

            xhr.setRequestHeader("Content-Type", "application/json");

            if (data == null) {
                xhr.send();
            }
            else {
                xhr.send(JSON.stringify(data));
            }
        }



    }   
}