﻿
using System.Web;
using System.Web.Optimization;

namespace bGomlaWebApp
{
    public static class BundelConfig
    {

       // private static readonly string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["CdnBaseUrl"];
        public static void RegesterBundels(BundleCollection bundels) 
        {



           // bundels.Add( new )

            bundels.Add(new StyleBundle("~/Theme/assets/global").Include(
            "~/Theme/assets/global/fonts.googleapis.com.family=OpenSans.css",
            "~/Theme/assets/global/plugins/font-awesome/css/font-awesome.min.css",
            "~/Theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
            "~/Theme/assets/global/plugins/bootstrap/css/bootstrap.min.css" ,
            "~/Theme/assets/global/plugins/uniform/css/uniform.default.css" ,
            "~/Theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
            "~/Theme/assets/global/MyStyles.css",
            "~/Theme/assets/global/plugins/gritter/css/jquery.gritter.css",
            "~/Theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css",
            "~/Theme/assets/global/plugins/jqvmap/jqvmap/jqvmap.css",
            "~/Theme/assets/admin/pages/css/tasks.css",
            "~/Theme/assets/global/css/components.css",
            "~/Theme/assets/global/css/plugins.css" ,
            "~/Theme/assets/admin/layout4/css/layout.css",
            "~/Theme/assets/admin/layout4/css/themes/light.css",
            "~/Theme/assets/admin/layout4/css/custom.css",
            "~/Theme/assets/global/plugins/fullcalendar/fullcalendar.css" 
               ));



            bundels.Add(new ScriptBundle("~/Content/Js").Include(

                 //      //  "~/Scripts/jquery-3.3.1.js",
                 //       "~/Theme/js/jquery-1.11.1.min.js",
                 //      "~/Scripts/jquery-3.3.1.js",
                 //        "~/Theme/js/jquery-migrate-1.2.1.min.js",
                 //        "~/Theme/js/jquery-ui-1.10.3.min.js",
                 //        "~/Theme/js/bootstrap.min.js",
                 //        "~/Theme/js/modernizr.min.js",
                 //        "~/Theme/js/jquery.sparkline.min.js",
                 //        "~/Theme/js/toggles.min.js",
                 //        "~/Theme/js/retina.min.js",
                 //        "~/Theme/js/jquery.cookies.js",
                 //        "~/Theme/js/flot/jquery.flot.min.js",
                 //        "~/Theme/js/flot/jquery.flot.resize.min.js",
                 //        "~/Theme/js/flot/jquery.flot.spline.min.js",
                 //        "~/Theme/js/morris.min.js",
                 //        "~/Theme/js/raphael-2.1.0.min.js",
                 //      //  "~/Theme/js/custom.js",
                 //        "~/Theme/js/jquery.validate.min.js",
                 //        "~/Theme/js/jquery.datatables.min.js",
                 //        "~/Theme/js/jquery.gritter.min.js",
                 //        "~/Theme/js/bootstrap-toggle.min.js",
                 //        "~/Scripts/App/app.js"
                 ////   "~/Theme/js/gmaps.js"



                
                 "~/Theme/assets/global/plugins/jquery-migrate.min.js",
              //   "~/Theme/assets/global/plugins/jquery.min.js",
                 "~/Theme/assets/global/plugins/jquery-ui/jquery-ui.min.js",
                 "~/Theme/assets/global/plugins/bootstrap/js/bootstrap.min.js",
                 "~/Theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js",
                 "~/Theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                 "~/Theme/assets/global/plugins/jquery.blockui.min.js",
                 "~/Theme/assets/global/plugins/jquery.cokie.min.js",
                 "~/Theme/assets/global/plugins/uniform/jquery.uniform.min.js",
                 "~/Theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
                 //"~/Theme/js/flot/jquery.flot.resize.min.js",
                 //"~/Theme/js/flot/jquery.flot.spline.min.js",
                 //"~/Theme/js/morris.min.js",
                 //"~/Theme/js/raphael-2.1.0.min.js",
                 //"~/Theme/js/custom.js",
                 //"~/Theme/js/jquery.validate.min.js",
                 //"~/Theme/js/jquery.datatables.min.js",
                 //"~/Theme/js/jquery.gritter.min.js",
                 //"~/Theme/js/bootstrap-toggle.min.js",
                 //"~/Theme/js/bootstrap-timepicker.min.js",
                 //"~/Scripts/jquery.signalR-2.2.3.min.js",
                 //"~/Scripts/App/app.js",
                 //"~/Scripts/App/Reports.js",
                 //"~/Scripts/jquery.playSound.js",
                 //"~/SignalR/hubs"
                 //    "~/Theme/js/toggles.min.js",
                 //   "~/Theme/js/gmaps.js"




         



         ));

            bundels.Add(new ScriptBundle("~/Content/ts").Include(
                        "~/appScriptsJS/Replication.js"));

            BundleTable.EnableOptimizations = false ;

        }

    }
}